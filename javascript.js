window.onload = generateStartPage; 
// level = 0;
highScore = 0;


function generateGamePage(event) {
	
	// console.log(event.target);
	// console.log(document.getElementById("b_restart"));

	// if this method is called by the restart button
	if (document.getElementById("b_restart") == event.target) {
		// console.log("IS b_restart");
	} else {
		// console.log("not b_restart");
		// Determine the level user selected
		if(document.getElementById('rblv1').checked) {
		  //Level 1 radio button is checked	
		  level = 1;
		  //console.log("generateGamePage() level = " + level);
		} 
		else if(document.getElementById('rblv2').checked) {
		  // Level 2 radio button is checked
		  //console.log("you are in level 2");
		  level = 2;
		  //console.log("generateGamePage() level = " + level);
		}

	}
	// console.log("level = "+level);
	// remove all elements in the body
	while( document.body.hasChildNodes() ) {
		document.body.removeChild(document.body.firstChild);
	}

	// create HTML elements
	var divInfobar = document.createElement("div");
	divInfobar.id = "infobar";

	var divTimer = document.createElement("div");
	divTimer.innerHTML = "Time: 60";
	divTimer.id = "myTimer";

	var divStop = document.createElement("div");
	divStop.innerHTML = "Pause";
	divStop.id = "pause";

	var divScore = document.createElement("div");
	divScore.innerHTML = "Score: 0";
	divScore.id = "score";

	var canvas = document.createElement("canvas");
	canvas.id = "myCanvas";
	canvas.width = 400;
	canvas.height = 600;

	var script = document.createElement("script");
	script.src = "javascript.js";
	script.type = "text/javascript";

	divInfobar.appendChild(divTimer);
	divInfobar.appendChild(divStop);
	divInfobar.appendChild(divScore);

	document.body.appendChild(divInfobar);
	document.body.appendChild(canvas);
	document.body.appendChild(script);

	init();
}


function generateStartPage() {
	// remove all elements in the body
	while( document.body.hasChildNodes() ) {
		document.body.removeChild(document.body.firstChild);
	}

	var divContainer = document.createElement("div");
	divContainer.id = 'container';
	var heading = document.createElement("h1");
	var p1 = document.createElement("p");
	var p2 = document.createElement("p");
	var p3 = document.createElement("p");
	var p4 = document.createElement("p");
	heading.innerHTML = "Tap Tap Bug!";
	p1.innerHTML = "There are 5 pieces of food randomly strewn about on the table. Every 1 to 3 seconds a new bug will crawl onto the table from the top of the screen.";
	p2.innerHTML = "Every bug will walk directly towards the closest piece of food on the table and eat it when it's close enough. Bugs once walking towards the eaten piece of food will change direction to the next closest piece of food.";
	p3.innerHTML = "There are three types of bugs in this game: black, red, and orange. Black bugs are the quickest, followed by the red bug, followed by the orange bug as the slowest.";
	p4.innerHTML = "You have 60 seconds to squash all the bugs before the timer runs out or all the food is eaten! Click on a bug to squash it. The faster the bug, the higher the points!";
	divContainer.appendChild(heading);
	divContainer.appendChild(p1);
	divContainer.appendChild(p2);
	divContainer.appendChild(p3);
	divContainer.appendChild(p4);
	
	// Radio Button
	var form = document.createElement("form");
	form.id = "radioButton";
	form.innerHTML = "Level: ";

	var label1 = document.createElement("label");
	var rb1 = document.createElement("input");
	rb1.type = "radio";
	rb1.name = "radioGroup";
	rb1.id = "rblv1";
	rb1.value = "level1";
	rb1.defaultChecked = true;
	rb1.checked = true;
	
	label1.appendChild(rb1);
	label1.appendChild(document.createTextNode("1   "));
	
	var label2 = document.createElement("label");
	var rb2 = document.createElement("input");
	rb2.type = "radio";
	rb2.name = "radioGroup";
	rb2.id = "rblv2";
	rb2.value = "level2";
	rb2.checked = false;
	
	label2.appendChild(rb2);
	label2.appendChild(document.createTextNode("2"));

	form.appendChild(label1);
	form.appendChild(label2);
	divContainer.appendChild(form);

	// Start Button
	var divButtonWrapper = document.createElement("div");
	divButtonWrapper.id = 'button_wrapper';
	var ul = document.createElement("ul");
	var li = document.createElement("li");
	var b1 = document.createElement("button");

	b1.innerHTML = "Start";
	b1.onclick = generateGamePage;

	divButtonWrapper.appendChild(ul);
	ul.appendChild(li);	
	li.appendChild(b1);	
	divContainer.appendChild(divButtonWrapper);

	// Set High Score
	var scoreBoard = document.createElement("p");
	if(localStorage.getItem("highScore")) {
		scoreBoard.innerHTML = "High Score: " + localStorage.getItem("highScore");	
	} else {
		scoreBoard.innerHTML = "High Score: " + 0;	
	}
	
	
	divContainer.appendChild(scoreBoard);

	var script = document.createElement("script");
	script.src = "javascript.js";
	script.type = "text/javascript";

	divContainer.appendChild(script);

	document.body.appendChild(divContainer);
}

// return the angle to rotate in radian
function getAngle(bugObject) {
	var closestFood = food_array[bugObject.shortest];
	var dy = closestFood.y - bugObject.y;
	var dx = closestFood.x - bugObject.x;
	var angle = Math.atan2( dy, dx ) - (90 * Math.PI / 180);
	
	return angle;
}

function init() {

	// Array to keep track of all bugs and food currently in the game
	bugs_array = [];
	food_array = [];

	// array of dead bugs that hasn't fully faded out
	deathOrder = [];
	
	// index for the closest food for a given bug
	index = 0;
	current = 0;
	
	// variables for timer
	time = 60;
	startTime = Date.now();
	timeElapsed = 0;
	
	// total score
	score = 0;

	// Boolean for pausing the game
	isPlaying = true; 
	
	// Get the canvas DOM
	c = document.getElementById("myCanvas"); 
	c.addEventListener("click", handleClick);
	ctx = c.getContext("2d");
		
	// run add an event listener for pressing a key
	document.addEventListener("keydown", keyDown, false); 
	
	//generate the food
	generate_food();
	
	// run the draw function every 10 milliseconds
	game = setInterval(draw, 10); 
	// window.requestAnimationFrame(draw);
	
	// Make a bug every 1 - 3 seconds
	bug_creator = setInterval(generate_bug, Math.floor(Math.random()*2000)+1000);
	
	//Pause the game if the pause is clicked
	document.getElementById("pause").onclick = pauseGame;
}

// draw a bug on canvas and make necessary adjustment whether it's dead or alive such as fading out and moving
function drawBug(bugObject) {

	var bugImg = new Image();
	var bugColor;
	var speed;
	if (bugObject.type < 3) {
		bugColor = "black";
		if (level === 2) {
			speed = 2;
		} else if ( level === 1) {
			speed = 1.5;
		}
	} else if (bugObject.type >= 3 && bugObject.type < 6) {
		bugColor = "red";
		if (level === 2) {
			speed = 1;
		} else if ( level === 1) {
			speed = 0.75;
		}

	} else if (bugObject.type >= 6 && bugObject.type < 10) {
		bugColor = "orange";
		if (level === 2) {
			speed = 0.8;
		} else if ( level === 1) {
			speed = 0.6;
		}
	}

	bugImg.src = "img/" + bugColor + "bug.png";

	ctx.save();
	ctx.translate(bugObject.x+5, bugObject.y+20);
	ctx.rotate(bugObject.angle);
	ctx.translate(-bugObject.x-5, -bugObject.y-20);
	ctx.globalAlpha = bugObject.opacity;
	ctx.drawImage(bugImg, bugObject.x, bugObject.y, 10, 40);
	ctx.restore();

	if (!bugObject.isAlive) {
		bugObject.opacity -= 0.005;
	} else { 
		bugObject.angle = getAngle(bugObject);

		// move bug
		if (bugObject.x < food_array[bugObject.shortest].x){ 
			bugObject.x += speed;
		}
		if (bugObject.x > food_array[bugObject.shortest].x){
			bugObject.x -= speed;
		}
		if (bugObject.y < food_array[bugObject.shortest].y){
			bugObject.y += speed;
		}
		if (bugObject.y > food_array[bugObject.shortest].y){
			bugObject.y -= speed;
		}	

	}


}

function draw(){

	// Clearing the entire canvas
	ctx.clearRect(0, 0, 400, 600); 
	
	// fade out killed bugs
	for (var j = 0; j < deathOrder.length; j++) {
		
		drawBug(deathOrder[j]);

	}
	// Draw Bugs that are alive
	for (var i = 0; i < bugs_array.length; i++){
			
		drawBug(bugs_array[i]);
					
		// If bug eats food
		if (bugs_array[i].x <= food_array[bugs_array[i].shortest].x + 3 && bugs_array[i].x >= food_array[bugs_array[i].shortest].x - 3 && 
			bugs_array[i].y <= food_array[bugs_array[i].shortest].y + 3 && bugs_array[i].y >= food_array[bugs_array[i].shortest].y - 3) 
			{
				food_array.splice(bugs_array[i].shortest, 1);

				// Check to see if the last food has been eaten, if it has, run the game_over function to signal the player has lost
				if (food_array.length == 0){ 
					game_over();
					return;
				}
				
				for (var j = 0; j < bugs_array.length; j++){
					bugs_array[j].shortest = shortest_to_bug(bugs_array[j].x, bugs_array[j].y, 0);					
				}
			}
	}

	// Draw 5 food
	for (var i = 0; i < food_array.length; i++){
		// display food image
		var foodImg = new Image();
		foodImg.src = "img/food.png";
		ctx.drawImage(foodImg, food_array[i].x, food_array[i].y, 20, 20);

	}

	// Display time remianing
	timer();

}

//Game over kill screen, need to implement a way to restart the game
function game_over(){

	//update high score
	if (score > localStorage.getItem("highScore")) {
		highScore = score;

		// save high score in the local storage
		if(typeof(Storage) !== "undefined") {
		    localStorage.setItem("highScore", highScore);
		} else {
		    // No Web Storage support
		}		
	}

	isPlaying = false;
	game = clearInterval(game);
	bug_creator = clearInterval(bug_creator);
	ctx.clearRect(0, 0, 400, 600); 	
	ctx.font="50px Georgia";
	ctx.textAlign = 'center';
	ctx.fillText("Game Over!", c.width/2, 100);	
	
	// remove canvas
	document.body.removeChild(c);

	// show score
	var curScore = document.createElement("h2");
	curScore.id = "currentScore";
	curScore.innerHTML = "Your Score: " + score;

	// restart button
	var restart = document.createElement("button");
	restart.id = "b_restart";
	restart.innerHTML = "Restart";
	restart.onclick = generateGamePage;

	// exit button
	var exit = document.createElement("button");
	exit.id = "b_exit";
	exit.innerHTML = "Exit";
	exit.onclick = generateStartPage;

	var endScreen = document.createElement("div");
	endScreen.id = "endScreen";
	endScreen.appendChild(curScore);
	endScreen.appendChild(restart);
	endScreen.appendChild(exit);
	document.body.appendChild(endScreen);
}

//Function for the timer
function timer(){
	timeElapsed = Date.now() - startTime;
	// console.log("timeElapsed " + timeElapsed);
	timeElapsedInSec = Math.floor(timeElapsed / 1000);
	// console.log("timeElapsedInSec " + timeElapsedInSec);

	var secRemaining = parseInt(time - timeElapsedInSec);
	// console.log("secRemaining is " + secRemaining);
	document.getElementById("myTimer").innerHTML = "Time: " + secRemaining;
	
	if (secRemaining == 0){
		game_over();
	}
	
}




function pauseGame() {
	
	//If the game is not paused, we pause the game by clearing the delay. Set pause boolean to true
  if (isPlaying) {
  	pauseTime = Date.now();
    game = clearInterval(game);
	bug_creator = clearInterval(bug_creator);
    isPlaying = false;
    document.getElementById("pause").innerHTML = "Play";

	//If the game is already paused, we unpause it by setting the interval again with the draw function. Set pause boolean to false
  } else{
  	resumeTime = Date.now();
  	durationPaused = resumeTime - pauseTime;
  	startTime += durationPaused;
  	// console.log("the game is paused for "+ durationPaused);
    game = setInterval(draw, 10);
	bug_creator = setInterval(generate_bug, Math.floor(Math.random()*2000)+1000);
    isPlaying = true;
    document.getElementById("pause").innerHTML = "Pause";
  }
}

function handleClick(event) {

	var rect = c.getBoundingClientRect();
	// where the user has clicked relative to the canvas whose origin being (0,0)
	var onCanvasClickX = event.clientX - rect.left;
	var onCanvasClickY = event.clientY - rect.top;
		
	//console.log("user clicked on = (" + event.clientX  + ", " + event.clientY  +")");
	//console.log("rect.left/top = (" + rect.left + 	", " + rect.top + ")");
	// console.log("c.offsetLeftTop = (" + c.offsetLeft  + ", " + c.offsetTop  +")");	
	//console.log("onCanvusClick = (" + onCanvasClickX +", " + onCanvasClickY);

	// go through the bug array and check if any bug is within the range (30px)
	for (var i = 0; i < bugs_array.length; i++) {
		// console.log("BUG ARRAY INDEX AT " + i);
		// calculate the distance from the centre of the bug and the click point
		var distance = getDistance(bugs_array[i].x+5, bugs_array[i].y+20, onCanvasClickX, onCanvasClickY);		
		// if range within 30px from the bug is clicked
		if (30 >= distance) {
			
			bugs_array[i].isAlive = false;
			updateScore(bugs_array[i].type);
					
			deathOrder.push(bugs_array[i]); // add the killed bug at the end
			bugs_array.splice(i, 1);	// remove the bug from the alive bug array
			i -= 1; // restart the search from the same index to prevent skipping the next bug that filled the place for the bug that just got removed
			// break;	can kill two bugs at once without break

			// remove bug from deathOrder after two seconds	
			setTimeout( function() {
							// console.log("2 sec after click");
							// remove from the array of dead bugs
							deathOrder.shift();
						}
				, 2000);
		}
	}
}

function updateScore(bugType) {
	if (bugType < 3) {	// black bug
		score += 5;
	} else if (bugType >= 3 && bugType < 6){ // red bug
		score += 3;
	} else if (bugType >= 6 && bugType < 10){ // orange bug
		score += 1;
	}
	document.getElementById("score").innerHTML = "Score: " + score;
}

//get the index of the food that is closest to the bug
function shortest_to_bug(bug_x, bug_y, shortest){
	
	if (isPlaying) {
		index = 0;
		current = getDistance(bug_x, bug_y, food_array[shortest].x, food_array[shortest].y);
		
		for (var i = 0; i < food_array.length; i++){
			if (getDistance(bug_x, bug_y, food_array[i].x, food_array[i].y) < current)
			{
				current = getDistance(bug_x, bug_y, food_array[i].x, food_array[i].y);
				index = i;
			}
		}
		return index;
	}
}


//Create a new bug, will call this periodically
function generate_bug(){
	new_bug = new bug (Math.floor(Math.random()*(400 - 10)) + 0, 0, bugs_array.length, Math.floor(Math.random()*9), 0);
	new_bug.shortest = shortest_to_bug(new_bug.x, new_bug.y, 0);
	bugs_array.push(new_bug);
}

//Create the food, will only be called once at the beginning of the game
function generate_food(){
	for (var i = 0; i < 5; i++){
		new_food = new food(Math.floor(Math.random()*(400 - 20)) + 0, Math.floor(Math.random()*(600 - 30 - 20)) + 30, food_array.length);
		food_array.push(new_food);
		// console.log("food " + i + " is at " + new_food.x + ", "+new_food.y)
	}
}

//Generate only one pieec of food for testing
function generate_food_test(){
	new_food = new food(Math.floor(Math.random()*(400 - 20)) + 0, Math.floor(Math.random()*(600 - 30 -20 )) + 30, food_array.length);
	food_array.push(new_food);
}


//Experimental Pause Feature taken from http://atomicrobotdesign.com/blog/web-development/pause-your-html5-canvas-game/
//Works so far

function keyDown(e) {
	//If the p key is pressed run pauseGame
  if (e.keyCode == 80) {
	  pauseGame();
  }
}

//Bug class
function bug (x, y, index, type, shortest){
		this.x = x;
		this.y = y;
		this.index = index;
		this.type = type;
		this.shortest = shortest;
		this.isAlive = true;
		this.opacity = 1;
		this.angle = 0;
}
	
//Food class
function food (x, y, index){
		this.x = x;
		this.y = y;
		this.index = index;
}


//distance formula, not sure if it's working	
function getDistance (x1, y1, x2, y2){
	var a = x1 - x2;
	var b = y1 - y2;
	return Math.sqrt(a*a + b*b);
}

